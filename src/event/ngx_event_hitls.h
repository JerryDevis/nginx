/*
 *  Copyright © openHiTLS
 */

#include "hitls_type.h"
#include "hitls_pki.h"
#include "bsl_log.h"
#include "crypt_eal_pkey.h"
#include "ngx_event_ssl.h"
#ifdef NGX_HITLS_SDF
#include "hitls_sdf/cert.h"
#endif

#define ngx_ssl_version() OPENHITLS_VERSION_S

#define ngx_ssl_verify_error_optional(n) 0

#define X509_V_OK 0

typedef HITLS_Config ngx_ssl_ctx_t;
typedef HITLS_Ctx ngx_ssl_conn_t;
typedef HITLS_X509_Cert ngx_x509_t;
typedef HITLS_Session ngx_ssl_session_t;
typedef CRYPT_EAL_PkeyCtx ngx_key_t;

#define ngx_ssl_error ngx_log_error
