/*
 *  Copyright © openHiTLS
 */


#include "ngx_event_hitls.h"
#include "bsl_err.h"
#include "bsl_uio.h"
#include "bsl_sal.h"
#include "crypt_algid.h"
#include "crypt_eal_rand.h"
#include "hitls.h"
#include "hitls_pki_errno.h"
#include "hitls_cert.h"
#include "hitls_cert_init.h"
#include "hitls_config.h"
#include "hitls_crypt_init.h"
#include "hitls_error.h"
#include "hitls_session.h"
#include "crypt_eal_pkey.h"
#include "crypt_eal_encode.h"
#include "bsl_list.h"
#include "cert_mgr.h"
#include "ngx_log.h"
#include "ngx_event.h"
#ifdef NGX_HITLS_SDF
#include "hitls_sdf/lib_load.h"
#include "hitls_sdf/cert.h"
#include "hitls_sdf/crypto_user_method.h"
#include "hitls_sdf/crypto.h"
#include "hitls_crypt_reg.h"
#endif

#define NGX_ERROR_IF_NULL(exp, log, tag) if ((exp) == NULL) { ngx_ssl_error(NGX_LOG_ALERT, (log), 0, tag); return NGX_ERROR;}
#define NGX_ERROR_IF_FAIL(exp, log, tag) if ((exp)) { ngx_ssl_error(NGX_LOG_ALERT, (log), 0, tag); return NGX_ERROR;}
#define GOTO_ERR_IF_FAIL(exp, log, tag) if ((exp)) { ngx_ssl_error(NGX_LOG_ALERT, (log), 0, tag); goto ERR;}

typedef struct {
    ngx_uint_t engine;
} ngx_hitls_conf_t;

static void *ngx_hitls_create_conf(ngx_cycle_t *cycle)
{
    ngx_hitls_conf_t *hitls_cf;
    hitls_cf = ngx_pcalloc(cycle->pool, sizeof(ngx_hitls_conf_t));
    if (hitls_cf == NULL)
        return NULL;
    return hitls_cf;
}

static ngx_core_module_t ngx_hitls_module_ctx = {
    ngx_string("hitls"),
    ngx_hitls_create_conf,
    NULL
};

ngx_module_t ngx_hitls_module = {
    NGX_MODULE_V1,
    &ngx_hitls_module_ctx,
    NULL,
    NGX_CORE_MODULE,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NGX_MODULE_V1_PADDING
};

static void ngx_ssl_clear_error(ngx_log_t *log)
{
    BSL_ERR_ClearError();
}

ngx_int_t ngx_ssl_shutdown(ngx_connection_t *c) 
{
    if (c->ssl->subject_dn.data) {
        BSL_SAL_FREE(c->ssl->subject_dn.data);
    }
    if (c->ssl->issuer_dn.data) {
        BSL_SAL_FREE(c->ssl->issuer_dn.data);
    }
    int ret = HITLS_Close(c->ssl->connection);
    BSL_UIO_Free(HITLS_GetUio(c->ssl->connection));
    HITLS_Free(c->ssl->connection);
    c->ssl->connection = NULL;
    return ret;
}

long ngx_ssl_get_verify_result(const ngx_ssl_conn_t *ssl) 
{
    fprintf(stderr, "[WARN] ngx_ssl_get_verify_result not implemented.\n");
    return 0;
}

ngx_int_t ngx_ssl_ocsp_get_status(ngx_connection_t *c, const char **s) 
{
    fprintf(stderr, "[WARN] OCSP Not supported.\n");
    return 0;
}

ngx_ssl_session_t *ngx_ssl_get0_session(ngx_connection_t *c) 
{
    return HITLS_GetSession(c->ssl->connection);
}

void ngx_ssl_remove_cached_session(ngx_ssl_ctx_t *ssl, ngx_ssl_session_t *sess)
{
    fprintf(stderr, "[WARN] ngx_ssl_remove_cached_session not implemented.\n");
}

const char *ngx_x509_verify_cert_error_string(long error_code)
{
    return BSL_ERR_GetString(error_code);
}

ngx_x509_t *ngx_ssl_get_peer_certificate(const ngx_ssl_conn_t *ssl)
{
    return HITLS_GetPeerCertificate(ssl);
}

void ngx_x509_free(ngx_x509_t *x509)
{
    HITLS_X509_CertFree(x509);
}

ngx_int_t ngx_ssl_create_connection(ngx_ssl_t *ssl, ngx_connection_t *c,
    ngx_uint_t flags)
{
    ngx_ssl_connection_t *sc;
    BSL_UIO *uio = NULL;

    // We will erenter here if TLS handshake receive recoverable exception like IO busy.
    // We won't initialize SSL context again if it is already initialized.
    if (c->ssl != 0) {
        return NGX_OK;
    }

    sc = ngx_pcalloc(c->pool, sizeof(ngx_ssl_connection_t));
    if (sc == NULL) {
        return NGX_ERROR;
    }

    sc->buffer = ((flags & NGX_SSL_BUFFER) != 0);
    sc->buffer_size = ssl->buffer_size;

    sc->session_ctx = ssl->ctx;

    //TODO SSL_READ_EARLY_DATA_SUCCESS

    NGX_ERROR_IF_NULL(
            sc->connection = HITLS_New((HITLS_Config *) ssl->ctx), c->log, "HITLS_New() failed");
    NGX_ERROR_IF_NULL(
            uio = BSL_UIO_New(BSL_UIO_TcpMethod()), c->log, "BSL_UIO_New() failed");
    GOTO_ERR_IF_FAIL(
            BSL_UIO_Ctrl(uio, BSL_UIO_SET_FD, sizeof(c->fd), &c->fd), c->log, "BSL_UIO_Ctrl(BSL_UIO_SET_FD) failed");
    GOTO_ERR_IF_FAIL(
            HITLS_SetUio(sc->connection, uio), c->log, "BSL_UIO_Ctrl(BSL_UIO_SET_FD) failed");

    if (flags & NGX_SSL_CLIENT) {
        sc->is_client = 1;
    }

    c->ssl = sc;
    return NGX_OK;

ERR:
    if (sc->connection) {
        HITLS_Free(sc->connection);
    }
    if (sc) {
        ngx_pfree(c->pool, sc);
    }
    if (uio) {
        BSL_UIO_Free(uio);
    }
    return NGX_ERROR;
}

ngx_int_t ngx_ssl_handshake(ngx_connection_t *c)
{
    int ret;
    
    if (c->ssl->is_client) {
        ret = HITLS_Connect((HITLS_Ctx *) c->ssl->connection);
    } else {
        ret = HITLS_Accept((HITLS_Ctx *) c->ssl->connection);
    }
    if (ret != HITLS_SUCCESS) {
        //TODO More error handling 
        if (ret == HITLS_REC_NORMAL_RECV_BUF_EMPTY || ret == HITLS_REC_NORMAL_IO_BUSY) {
            return NGX_AGAIN;
        }
        ngx_ssl_error(NGX_LOG_ALERT, c->log, 0, "HITLS_Accept failed");
        return NGX_ERROR;
    }

    if (ngx_handle_read_event(c->read, 0) != NGX_OK) {
        return NGX_ERROR;
    }

    if (ngx_handle_write_event(c->write, 0) != NGX_OK) {
        return NGX_ERROR;
    }

    c->recv = ngx_ssl_recv;
    c->send = ngx_ssl_write;
    c->recv_chain = ngx_ssl_recv_chain;
    c->send_chain = ngx_ssl_send_chain;

    c->read->ready = 1;
    c->write->ready = 1;

    //TODO validate OCSP

    c->ssl->handshaked = 1;
    return NGX_OK;
}

static ngx_int_t ngx_ssl_handle_recv(ngx_connection_t *c, int n, int sslerr)
{
    if (sslerr == HITLS_SUCCESS && n > 0) {

        if (c->ssl->saved_write_handler) {

            c->write->handler = c->ssl->saved_write_handler;
            c->ssl->saved_write_handler = NULL;
            c->write->ready = 1;

            if (ngx_handle_write_event(c->write, 0) != NGX_OK) {
                return NGX_ERROR;
            }

            ngx_post_event(c->write, &ngx_posted_events);
        }

        return NGX_OK;
    }

    ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0, "SSL_get_error: %d", sslerr);

    if (sslerr == HITLS_REC_NORMAL_RECV_BUF_EMPTY) {

        if (c->ssl->saved_write_handler) {

            c->write->handler = c->ssl->saved_write_handler;
            c->ssl->saved_write_handler = NULL;
            c->write->ready = 1;

            if (ngx_handle_write_event(c->write, 0) != NGX_OK) {
                return NGX_ERROR;
            }

            ngx_post_event(c->write, &ngx_posted_events);
        }

        c->read->ready = 0;
        return NGX_AGAIN;
    }

    if (sslerr == HITLS_WANT_WRITE) {

        ngx_log_debug0(NGX_LOG_DEBUG_EVENT, c->log, 0,
                       "SSL_read: want write");

        c->write->ready = 0;

        if (ngx_handle_write_event(c->write, 0) != NGX_OK) {
            return NGX_ERROR;
        }

        /*
         * we do not set the timer because there is already the read event timer
         */

        if (c->ssl->saved_write_handler == NULL) {
            c->ssl->saved_write_handler = c->write->handler;
            c->write->handler = ngx_ssl_write_handler;
        }

        return NGX_AGAIN;
    }

    c->ssl->no_wait_shutdown = 1;
    c->ssl->no_send_shutdown = 1;

    ngx_ssl_error(NGX_LOG_ERR, c->log, 0, "SSL_read() failed");

    return NGX_ERROR;
}

ssize_t ngx_ssl_recv(ngx_connection_t *c, u_char *buf, size_t size)
{
    unsigned int n = 0;

    if (c->ssl->last == NGX_ERROR) {
        c->read->ready = 0;
        c->read->error = 1;
        return NGX_ERROR;
    }

    if (c->ssl->last == NGX_DONE) {
        c->read->ready = 0;
        c->read->eof = 1;
        return 0;
    }

    int bytes = 0;

    ngx_ssl_clear_error(c->log);

    /*
     * SSL_read() may return data in parts, so try to read
     * until SSL_read() would return no data
     */

    for ( ;; ) {

        int ret = HITLS_Read(c->ssl->connection, buf, size, &n);

        ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0, "SSL_read: %d", n);

        if (n > 0) {
            bytes += n;
        }

        c->ssl->last = ngx_ssl_handle_recv(c, n, ret);

        if (c->ssl->last == NGX_OK) {

            size -= n;

            if (size == 0) {
                c->read->ready = 1;

                if (c->read->available >= 0) {
                    c->read->available -= bytes;

                    /*
                     * there can be data buffered at SSL layer,
                     * so we post an event to continue reading on the next
                     * iteration of the event loop
                     */

                    if (c->read->available < 0) {
                        c->read->available = 0;
                        c->read->ready = 0;

                        if (c->read->posted) {
                            ngx_delete_posted_event(c->read);
                        }

                        ngx_post_event(c->read, &ngx_posted_next_events);
                    }

                    ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0,
                                   "SSL_read: avail:%d", c->read->available);

                } else {

#if (NGX_HAVE_FIONREAD)

                    if (ngx_socket_nread(c->fd, &c->read->available) == -1) {
                        c->read->ready = 0;
                        c->read->error = 1;
                        ngx_connection_error(c, ngx_socket_errno,
                                             ngx_socket_nread_n " failed");
                        return NGX_ERROR;
                    }

                    ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0,
                                   "SSL_read: avail:%d", c->read->available);

#endif
                }

                return bytes;
            }

            buf += n;

            continue;
        }

        if (bytes) {
            if (c->ssl->last != NGX_AGAIN) {
                c->read->ready = 1;
            }

            return bytes;
        }

        switch (c->ssl->last) {

        case NGX_DONE:
            c->read->ready = 0;
            c->read->eof = 1;
            return 0;

        case NGX_ERROR:
            c->read->ready = 0;
            c->read->error = 1;

            /* fall through */

        case NGX_AGAIN:
            return c->ssl->last;
        }
    }

}

ssize_t ngx_ssl_write(ngx_connection_t *c, u_char *data, size_t size)
{
    uint32_t n = 0;
    int32_t ret, sslerr;
    ngx_ssl_clear_error(c->log);

    ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0, "SSL to write: %uz", size);

    ret = HITLS_Write(c->ssl->connection, data, size, &n);

    ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0, "SSL_write: %u", n);

    if (ret == HITLS_SUCCESS) {

        if (c->ssl->saved_read_handler) {

            c->read->handler = c->ssl->saved_read_handler;
            c->ssl->saved_read_handler = NULL;
            c->read->ready = 1;

            if (ngx_handle_read_event(c->read, 0) != NGX_OK) {
                return NGX_ERROR;
            }

            ngx_post_event(c->read, &ngx_posted_events);
        }

        c->sent += size;

        return size;
    }

    ngx_log_debug1(NGX_LOG_DEBUG_EVENT, c->log, 0, "SSL_get_error: %d", ret);
    sslerr = HITLS_GetError(c->ssl->connection, ret);
    if (sslerr == HITLS_WANT_WRITE) {

        if (c->ssl->saved_read_handler) {

            c->read->handler = c->ssl->saved_read_handler;
            c->ssl->saved_read_handler = NULL;
            c->read->ready = 1;

            if (ngx_handle_read_event(c->read, 0) != NGX_OK) {
                return NGX_ERROR;
            }

            ngx_post_event(c->read, &ngx_posted_events);
        }

        c->write->ready = 0;
        return NGX_AGAIN;
    }

    if (sslerr == HITLS_WANT_READ) {

        ngx_log_debug0(NGX_LOG_DEBUG_EVENT, c->log, 0,
                       "SSL_write: want read");

        c->read->ready = 0;

        if (ngx_handle_read_event(c->read, 0) != NGX_OK) {
            return NGX_ERROR;
        }

        /*
         * we do not set the timer because there is already
         * the write event timer
         */

        if (c->ssl->saved_read_handler == NULL) {
            c->ssl->saved_read_handler = c->read->handler;
            c->read->handler = ngx_ssl_read_handler;
        }

        return NGX_AGAIN;
    }

    c->ssl->no_wait_shutdown = 1;
    c->ssl->no_send_shutdown = 1;
    c->write->error = 1;

    ngx_ssl_error(NGX_LOG_ERR, c->log, 0, "SSL_write() failed");

    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_check_host(ngx_connection_t *c, ngx_str_t *name)
{
    ngx_ssl_error(NGX_LOG_ERR, c->log, 0, "ngx_ssl_check_host not implemented");
    //TODO verify cert CN
    return NGX_OK;
}

ngx_int_t
ngx_ssl_connection_certificate(ngx_connection_t *c, ngx_pool_t *pool,
    ngx_str_t *cert, ngx_str_t *key, ngx_array_t *passwords)
{
    ngx_ssl_error(NGX_LOG_ERR, c->log, 0, "ngx_ssl_connection_certificate not implemented");
    return NGX_ERROR;
}
 
ngx_ssl_session_t *ngx_d2i_ssl_session(ngx_ssl_session_t **a, const unsigned char **pp, long len)
{
    fprintf(stderr, "ngx_d2i_ssl_session not implemented.\n");
    return 0;
}

ngx_int_t
ngx_ssl_set_session(ngx_connection_t *c, ngx_ssl_session_t *session)
{
    if (session) {
        NGX_ERROR_IF_FAIL(HITLS_SetSession(c->ssl->connection, session), c->log, "HITLS_SetSession failed");
    }

    return NGX_OK;
}

void ngx_ssl_free_session(ngx_ssl_session_t *session)
{
    HITLS_SESS_Free(session);
}

int ngx_i2d_ssl_session(ngx_ssl_session_t *in, unsigned char **pp)
{
    fprintf(stderr, "ngx_i2d_ssl_session not implemented.\n");
    return 0;
}

ngx_ssl_session_t *ngx_ssl_get_session(ngx_connection_t *c)
{
    return HITLS_GetDupSession(c->ssl->connection);
}

ngx_int_t ngx_ssl_ocsp_cache_init(ngx_shm_zone_t *shm_zone, void *data)
{
    fprintf(stderr, "ngx_ssl_ocsp_cache_init not implemented.\n");
    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_session_cache(ngx_ssl_t *ssl, ngx_str_t *sess_ctx,
    ngx_array_t *certificates, ssize_t builtin_session_cache,
    ngx_shm_zone_t *shm_zone, time_t timeout)
{
    HITLS_Config *config = (HITLS_Config *) ssl->ctx;
    if (builtin_session_cache < 0 && builtin_session_cache != NGX_SSL_NONE_SCACHE) {
        return NGX_ERROR;
    }
    if (HITLS_CFG_SetSessionTimeout(config, timeout) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }
    if (builtin_session_cache == NGX_SSL_NONE_SCACHE) {
        if (HITLS_CFG_SetSessionCacheMode(config, HITLS_SESS_CACHE_NO) != HITLS_SUCCESS) {
            return NGX_ERROR;
        }
        return NGX_OK;
    }
    if (HITLS_CFG_SetFlightTransmitSwitch(config, true) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }

    if (HITLS_CFG_SetSessionTicketSupport(config, false) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }
    if (HITLS_CFG_SetSessionCacheMode(config, HITLS_SESS_CACHE_SERVER) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }
    if (HITLS_CFG_SetSessionCacheSize(config, builtin_session_cache) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }
    return NGX_OK;
}

ngx_int_t ngx_ssl_session_cache_init(ngx_shm_zone_t *shm_zone, void *data)
{
    fprintf(stderr, "ngx_ssl_session_cache_init not implemented.\n");
    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_stapling_resolver(ngx_conf_t *cf, ngx_ssl_t *ssl,
    ngx_resolver_t *resolver, ngx_msec_t resolver_timeout)
{
    return NGX_OK;
}

ngx_int_t
ngx_ssl_ocsp_resolver(ngx_conf_t *cf, ngx_ssl_t *ssl,
    ngx_resolver_t *resolver, ngx_msec_t resolver_timeout)
{
    return NGX_OK;
}

#ifdef NGX_HITLS_SDF
extern HITLS_CRYPT_BaseMethod g_cryptBaseMethod;
extern HITLS_CERT_MgrMethod g_certMgrMethod;
#endif

ngx_int_t
ngx_ssl_create(ngx_ssl_t *ssl, ngx_uint_t protocols, void *data)
{
    BSL_ERR_Init();
    HITLS_CertMethodInit();
    HITLS_CryptMethodInit();
    fprintf(stderr, "CRYPT_EAL_RandInit start.\n");
    if (CRYPT_EAL_RandInit(CRYPT_RAND_SHA256, NULL, NULL, NULL, 0) != HITLS_SUCCESS) {
        fprintf(stderr, "CRYPT_EAL_RandInit failed.\n");
        return NGX_ERROR;
    }
    fprintf(stderr, "CRYPT_EAL_RandInit complete.\n");

#ifdef NGX_HITLS_SDF
    if (ssl->sdf) {
        if (HITLS_SDF_lib_load((char *) ssl->sdf_lib_path.data)) {
            fprintf(stderr, "SDF lib load failed.\n");
            return NGX_ERROR;
        }
        if (HITLS_SDF_init_global_session()) {
            fprintf(stderr, "SDF init session failed.\n");
            return NGX_ERROR;
        }
        g_cryptBaseMethod.randBytes = HITLS_SDF_RAND_rand_bytes;
        g_certMgrMethod.keyCtrl = (CERT_KeyCtrlCallBack) HITLS_SDF_CERT_KeyCtrl;
        g_cryptBaseMethod.checkPrivateKey = (CERT_CheckPrivateKeyCallBack) HITLS_SDF_CERT_CheckPrivateKey;

        CRYPT_EAL_MdRegMethod(CRYPT_MD_SM3, HITLS_SDF_GetMdUserMethod());
        CRYPT_EAL_MacRegMethod(CRYPT_MAC_HMAC_SM3, HITLS_SDF_GetMacUserMethod());
        CRYPT_EAL_CipherRegMethod(CRYPT_CIPHER_SM4_CBC, HITLS_SDF_GetCipherUserMethod());
        CRYPT_EAL_PkeyRegMethod(CRYPT_PKEY_SM2, HITLS_SDF_GetPKeyUserMethod());
    }
#endif

    ssl->protocols = protocols;
    if (protocols == NGX_SSL_TLCP) {
        ssl->ctx = HITLS_CFG_NewTLCPConfig();
    } else {
        ssl->ctx = HITLS_CFG_NewTLS12Config();
    }
    if (ssl->ctx == NULL) {
        fprintf(stderr, "new ssl config failed.\n");
        return NGX_ERROR;
    }
    HITLS_CFG_SetCheckKeyUsage(ssl->ctx, false);

    //nginx close extendkey
    HITLS_CFG_SetExtenedMasterSecretSupport(ssl->ctx, false);

    return NGX_OK;
}

void ngx_ssl_cleanup_ctx(void *data)
{
    ngx_ssl_t  *ssl = data;

    HITLS_CFG_FreeConfig(ssl->ctx);
}

ngx_int_t
ngx_ssl_ciphers(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *ciphers,
    ngx_uint_t prefer_server_ciphers)
{
    if (prefer_server_ciphers) {
        HITLS_CFG_SetCipherServerPreference(ssl->ctx, true);
    }
    return NGX_OK;
}

ngx_array_t *
ngx_ssl_preserve_passwords(ngx_conf_t *cf, ngx_array_t *passwords)
{
    static ngx_array_t empty_passwords;
    fprintf(stderr, "ngx_ssl_preserve_passwords not implemented.\n");
    return &empty_passwords;
}

static ngx_x509_t *LoadCertListAndCert(ngx_str_t *file, BslList **certList)
{
    if (file == NULL || certList == NULL) {
        return NULL;
    }
    int32_t ret = HITLS_X509_CertMulParseFile(BSL_FORMAT_PEM, (const char *)file->data, certList);
    if (ret != HITLS_X509_SUCCESS) {
        return NULL;
    }
    if (*certList == NULL || BSL_LIST_COUNT(*certList) == 0) {
        return NULL;
    }
    ngx_x509_t *cert = BSL_LIST_GET_FIRST(*certList);
    if (cert == NULL) {
        BSL_LIST_FREE(*certList, (BSL_LIST_PFUNC_FREE)HITLS_X509_CertFree);
        return NULL;
    }
    BslListNode *detachNode = BSL_LIST_FirstNode(*certList);
    BSL_LIST_DetachNode(*certList, &detachNode);
    if (BSL_LIST_COUNT(*certList) == 0) {
        BSL_LIST_FREE(*certList, (BSL_LIST_PFUNC_FREE)HITLS_X509_CertFree);
    }
    return cert;
}

static HITLS_CERT_Store *BuildCertStoreFromList(BslList *certList)
{
    HITLS_CERT_Store *x509Store = HITLS_X509_StoreCtxNew();
    if (x509Store == NULL) {
        return NULL;
    }
    int32_t ret;
    ngx_x509_t *cert = BSL_LIST_GET_FIRST(certList);
    while (cert != NULL) {
        ret = HITLS_X509_StoreCtxCtrl(x509Store, HITLS_X509_STORECTX_DEEP_COPY_SET_CA, cert, 0);
        if (ret != HITLS_SUCCESS) {
            HITLS_X509_StoreCtxFree(x509Store);
            return NULL;
        }
        cert = BSL_LIST_GET_NEXT(certList);
    }
    return x509Store;
}

static int SetCertListToChainStore(ngx_ssl_t *ssl, BslList *certList)
{
    HITLS_CERT_Store *chainStore = BuildCertStoreFromList(certList);
    if (chainStore == NULL) {
        fprintf(stderr, "Failed to build cert store from list.\n");
        return NGX_ERROR;
    }

    if (HITLS_CFG_SetChainStore(ssl->ctx, chainStore, false) != HITLS_SUCCESS) {
        fprintf(stderr, "Failed to set chain store.\n");
        HITLS_X509_StoreCtxFree(chainStore);
        return NGX_ERROR;
    }
    return NGX_OK;
}

static int SetCertListToCertStore(ngx_ssl_t *ssl, BslList *certList)
{
    HITLS_CERT_Store *certStore = BuildCertStoreFromList(certList);
    if (certStore == NULL) {
        ngx_log_error(NGX_LOG_ERR, ssl->log, 0, "build x509 store context fail");
        return NGX_ERROR;
    }
    if (HITLS_CFG_SetCertStore(ssl->ctx, certStore, false) != HITLS_SUCCESS) {
        HITLS_X509_StoreCtxFree(certStore);
        return NGX_ERROR;
    }

    return NGX_OK;
}

static int sign_cert_loaded = 0;
static int setTlcpCertificateAndKey(ngx_ssl_t *ssl, ngx_x509_t *cert_x509, HITLS_CERT_Key *key)
{
    int ret = 0;

    NGX_ERROR_IF_FAIL(HITLS_CFG_SetTlcpCertificate(ssl->ctx, cert_x509, false, sign_cert_loaded), ssl->log,
        "HITLS_CFG_SetTlcpCertificate failed.");

    ret = HITLS_CFG_SetTlcpPrivateKey(ssl->ctx, key, false, sign_cert_loaded);
    if (ret != HITLS_SUCCESS) {
        if (ret == HITLS_CERT_ERR_CHECK_CERT_AND_KEY) {
            ngx_log_error(NGX_LOG_ERR, ssl->log, 0, "Tlcp certificate and key checked failed");
        } else {
            ngx_log_error(NGX_LOG_ERR, ssl->log, 0, "HITLS_CFG_SetTlcpPrivateKey failed 0x%x.", ret);
        }
        return NGX_ERROR;
    }
    sign_cert_loaded = 1;
    return NGX_OK;
}

static int setTlsCertificateAndKey(ngx_ssl_t *ssl, ngx_x509_t *cert_x509, HITLS_CERT_Key *key)
{
    int ret = 0;
    if (HITLS_CFG_SetCertificate(ssl->ctx, cert_x509, false) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }
    if ((ret = HITLS_CFG_SetPrivateKey(ssl->ctx, key, false)) != HITLS_SUCCESS) {
        if (ret == HITLS_CERT_ERR_CHECK_CERT_AND_KEY) {
            ngx_log_error(NGX_LOG_ERR, ssl->log, 0, "TLS Certificate and Key check failed.");
        } else {
            ngx_log_error(NGX_LOG_ERR, ssl->log, 0, "HITLS_CFG_SetPrivateKey failed 0x%x.", ret);
        }
        return NGX_ERROR;
    }
    return NGX_OK;
}

ngx_int_t
ngx_ssl_certificates(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_array_t *certs,
    ngx_array_t *keys, ngx_array_t *passwords)
{
    ngx_str_t *cert, *key;
    ngx_uint_t i;

    cert = certs->elts;
    key = keys->elts;

    if (ssl->protocols == NGX_SSL_TLCP && certs->nelts != 2) {
        if (certs->nelts == 1) {
            ngx_log_error(NGX_LOG_ERR, ssl->log, 0, 
                "Missing the ENC certificate for TLCP.");
        } else {
            ngx_log_error(NGX_LOG_ERR, ssl->log, 0, 
                "Invalid number of certificates for TLCP: %d.", certs->nelts);
        }
    }
    for (i = 0; i < certs->nelts; i++) {

        if (ngx_ssl_certificate(cf, ssl, &cert[i], &key[i], passwords)
            != NGX_OK)
        {
            return NGX_ERROR;
        }
    }

    return NGX_OK;
}

static ngx_key_t *ParseFilePriKey(const char *path, uint8_t *pwd, uint32_t pwdlen)
{
    static int32_t tryTypes[] = { CRYPT_PRIKEY_PKCS8_UNENCRYPT, CRYPT_PRIKEY_PKCS8_ENCRYPT, CRYPT_PRIKEY_RSA,
        CRYPT_PRIKEY_ECC };
    int32_t ret;
    ngx_key_t *ealPriKey = NULL;
    for (size_t i = 0; i < sizeof(tryTypes) / sizeof(tryTypes[0]); i++) {
        ret = CRYPT_EAL_DecodeFileKey(BSL_FORMAT_PEM, tryTypes[i], path, pwd, pwdlen,
            (ngx_key_t **)&ealPriKey);
        if (ret == HITLS_SUCCESS) {
            return ealPriKey;
        }
    }
    return NULL;
}

ngx_int_t ngx_ssl_certificate(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *cert,
    ngx_str_t *key, ngx_array_t *passwords)
{
    int ret = 0;
    ngx_x509_t *cert_x509 = NULL;
    ngx_key_t *cert_key = NULL;
    uint8_t *pass = NULL;
    uint32_t passLen = 0;
    int success = 0;
    BslList *certList = NULL;

    NGX_ERROR_IF_NULL(cert_x509 = LoadCertListAndCert(cert, &certList), ssl->log, "Load cert failed.");
    /* Set certificate chain to chainStore */
    if (certList != NULL) {
        GOTO_ERR_IF_FAIL(SetCertListToChainStore(ssl, certList), ssl->log, "Set cert list to chain store failed");
    }
#ifdef NGX_HITLS_SDF
    if (ssl->sdf) {
        if (!sign_cert_loaded) {
            fprintf(stderr, "Load sdf key %s.\n", key->data);
            GOTO_ERR_IF_FAIL(HITLS_CFG_SetTlcpCertificate(ssl->ctx, cert_x509, false, sign_cert_loaded), ssl->log,
                "HITLS_CFG_SetTlcpCertificate failed.");

            pass = (const char *)((ngx_str_t *) passwords->elts)->data;
            passLen = ((ngx_str_t *) passwords->elts)->len;
            GOTO_ERR_IF_FAIL(ssl->sdf_key = HITLS_SDF_CERT_KeyParse((const char *)key->data, key->len,
                pass, passLen), ssl->log, "HITLS_SDF_CERT_KeyParse failed.");

            GOTO_ERR_IF_FAIL(HITLS_CFG_SetTlcpPrivateKey(ssl->ctx, ssl->sdf_key, false, sign_cert_loaded), ssl->log,
                "HITLS_CFG_SetTlcpPrivateKey failed.");
            sign_cert_loaded = 1;
        } else {
            GOTO_ERR_IF_FAIL(HITLS_CFG_SetTlcpCertificate(ssl->ctx, cert_x509, false, sign_cert_loaded), ssl->log,
                "HITLS_CFG_SetTlcpCertificate failed.");
            GOTO_ERR_IF_FAIL(HITLS_CFG_SetTlcpPrivateKey(ssl->ctx, ssl->sdf_key, false, sign_cert_loaded), ssl->log,
                "HITLS_CFG_SetTlcpPrivateKey failed.");
            // Adding refCount to key so that ssl->sdf_key is not freed twice.
            CRYPT_EAL_PkeyCtx *ctx = ssl->sdf_key;
            HITLS_SDF_UserPkeyCtx *key = ctx->key;
            key->refCount++;
        }
        success = 1;
        goto ERR;
    }
#endif
    if (passwords != NULL && passwords->elts != NULL) {
        pass = ((ngx_str_t *)passwords->elts)->data;
        passLen = ((ngx_str_t *)passwords->elts)->len;
    }

    cert_key = ParseFilePriKey((const char *)key->data, pass, passLen);
    if (cert_key == NULL) {
        goto ERR;
    }
    if (ssl->protocols == NGX_SSL_TLCP) {
        GOTO_ERR_IF_FAIL(setTlcpCertificateAndKey(ssl, cert_x509, cert_key), ssl->log,
            "setTlcpCertificateAndKey failed");
    } else {
        ret = setTlsCertificateAndKey(ssl, cert_x509, cert_key);
        if (ret != NGX_OK) {
            goto ERR;
        }
    }
    success = 1;
ERR:
    if (success) {
        return NGX_OK;
    } else {
        if (cert_x509 != NULL) {
            HITLS_X509_CertFree(cert_x509);
        }
        return NGX_ERROR;
    }
}

ngx_int_t
ngx_ssl_client_certificate(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *cert,
    ngx_int_t depth)
{
    BslList *certList = NULL;

    NGX_ERROR_IF_FAIL(HITLS_CFG_SetVerifyDepth(ssl->ctx, depth), ssl->log, "HITLS_CFG_SetVerifyDepth failed");
    if (cert != NULL && cert->data != NULL) {
        NGX_ERROR_IF_FAIL(HITLS_X509_CertMulParseFile(BSL_FORMAT_PEM, (const char *)cert->data, &certList), 
            ssl->log, "HITLS_X509_CertMulParseFile failed");
    }
    if (certList != NULL) {
        NGX_ERROR_IF_FAIL(SetCertListToCertStore(ssl, certList), ssl->log, "Set cert store failed");
    }

    NGX_ERROR_IF_FAIL(HITLS_CFG_SetClientVerifySupport(ssl->ctx, true), ssl->log,
        "HITLS_CFG_SetClientVerifySupport failed");
    /* add ca distinguish name to ca_list */
    return NGX_OK;
}

ngx_int_t
ngx_ssl_trusted_certificate(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *cert,
    ngx_int_t depth)
{
    BslList *certList = NULL;

    if (cert->len == 0) return NGX_OK;

    NGX_ERROR_IF_FAIL(HITLS_CFG_SetVerifyDepth(ssl->ctx, depth), ssl->log, "HITLS_CFG_SetVerifyDepth failed");
    if (cert != NULL && cert->data != NULL) {
        NGX_ERROR_IF_FAIL(HITLS_X509_CertMulParseFile(BSL_FORMAT_PEM, (const char *)cert->data, &certList), 
            ssl->log, "HITLS_X509_CertMulParseFile failed");
    }
    if (certList != NULL) {
        NGX_ERROR_IF_FAIL(SetCertListToCertStore(ssl, certList), ssl->log, "Set cert store failed");
    }
    NGX_ERROR_IF_FAIL(HITLS_CFG_SetClientVerifySupport(ssl->ctx, true), ssl->log, "HITLS_CFG_SetClientVerifySupport");

    return NGX_OK;
}

ngx_int_t
ngx_ssl_crl(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *crl)
{
    ngx_log_error(NGX_LOG_ERR, ssl->log, 0, "ngx_ssl_crl not implemented.\n");
    return NGX_OK;
}

ngx_int_t
ngx_ssl_ocsp(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *responder,
    ngx_uint_t depth, ngx_shm_zone_t *shm_zone)
{
    ngx_log_error(NGX_LOG_EMERG, ssl->log, 0,
                  "\"ssl_ocsp\" is not supported on this platform");

    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_dhparam(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *file)
{
    if (file->len == 0) {
        return NGX_OK;
    }
    ngx_log_error(NGX_LOG_EMERG, ssl->log, 0,
                  "\"dh_param\" is not supported on this platform");
    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_ecdh_curve(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *name)
{
    if (name->len == 4 && strcmp((const char *) name->data, "auto") == 0) {
        return NGX_OK;
    }
    ngx_log_error(NGX_LOG_EMERG, ssl->log, 0,
                  "\"ecdh_curve\" is not supported on this platform");

    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_session_ticket_keys(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_array_t *paths)
{
    if (paths) {
        ngx_log_error(NGX_LOG_WARN, ssl->log, 0,
                      "\"ssl_session_ticket_key\" ignored, not supported");
    }

    return NGX_OK;
}

ngx_int_t
ngx_ssl_stapling(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_str_t *file,
    ngx_str_t *responder, ngx_uint_t verify)
{
    ngx_log_error(NGX_LOG_WARN, ssl->log, 0,
                  "\"ssl_stapling\" ignored, not supported");

    return NGX_OK;
}

ngx_int_t
ngx_ssl_early_data(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_uint_t enable)
{
    if (!enable) {
        return NGX_OK;
    }
    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_conf_commands(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_array_t *commands)
{
    if (commands == NULL) {
        return NGX_OK;
    }
    return NGX_ERROR;
}

ngx_int_t
ngx_ssl_get_protocol(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    HITLS_Ctx *ctx;
    HITLS_Session *session;
    unsigned short protocol;

    ctx = c->ssl->connection;
    session = HITLS_GetSession(ctx);
    if (session == NULL)
        goto error;

    if (HITLS_SESS_GetProtocolVersion(session, &protocol))
        goto error;

    if (protocol == HITLS_VERSION_TLCP11) {
        s->data = (unsigned char *)"TLCP";
    } else {
        s->data = (unsigned char *)"TLS1.2";
    }
    return NGX_OK;
error:
    s->data = (unsigned char *) "";
    s->len = 1;
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_cipher_name(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    unsigned short cipherSuite;
    HITLS_Session *session;

    session = HITLS_GetSession(c->ssl->connection);
    if (session == 0) {
        return NGX_ERROR;
    }

    if (HITLS_SESS_GetCipherSuite(session, &cipherSuite) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }

    switch (cipherSuite) {
    case HITLS_ECDHE_SM4_CBC_SM3:
    case HITLS_ECC_SM4_CBC_SM3:
        s->data = (unsigned char *) "SM4_CBC";
        break;
    default:
        s->data = ngx_pnalloc(pool, 10);
        sprintf((char *) s->data, "0x%x", cipherSuite);
    }

    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_ciphers(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING ngx_ssl_get_ciphers implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_curve(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_curves(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_session_id(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    HITLS_Ctx *ctx;
    HITLS_Session *session;
    unsigned char sessionId[32];
    unsigned int sessionIdLen = 32;

    ctx = c->ssl->connection;
    session = HITLS_GetSession(ctx);
    if (session == NULL)
        goto error;

    if (HITLS_SESS_GetSessionId(session, sessionId, &sessionIdLen))
        goto error;

    s->data = ngx_pnalloc(pool, 64);
    for (unsigned int i = 0; i < sessionIdLen; i++) {
        char *cursor = (char *) s->data + i * 2;
        sprintf(cursor, "%02x", sessionId[i]);
    }
    s->len = strlen((const char *) s->data);

    return NGX_OK;
error:
    s->data = (unsigned char *) "";
    s->len = 1;
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_session_reused(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    HITLS_Ctx *ctx;
    unsigned char reused;

    ctx = c->ssl->connection;
    if (HITLS_IsSessionReused(ctx, &reused))
        goto error;

    if (reused) {
        s->data = (unsigned char *) "r";
    } else {
        s->data = (unsigned char *) ".";
    }

    return NGX_OK;
error:
    s->data = (unsigned char *) "";
    s->len = 1;
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_early_data(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_server_name(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_alpn_protocol(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_certificate(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_raw_certificate(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_escaped_certificate(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    ngx_str_t cert;
    uintptr_t n;
    HITLS_CERT_X509 *clientCert = NULL;
    int success = 0;
    cert.data = NULL;
    cert.len = 0;
    clientCert = HITLS_GetPeerCertificate(c->ssl->connection);
    if (clientCert == NULL) {
        goto error;
    }
    int32_t ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_ENCODELEN, &cert.len,
        (int32_t)sizeof(uint32_t));
    if (ret != HITLS_SUCCESS) {
        goto error;
    }
    
    ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_ENCODE, (void *)&cert.data, 0);
    if (ret != HITLS_SUCCESS) {
        goto error;
    }

    n = ngx_escape_uri(NULL, cert.data, cert.len, NGX_ESCAPE_URI_COMPONENT);
    s->len = cert.len + n * 2;
    s->data = ngx_pnalloc(pool, s->len);
    if (s->data == NULL) {
        goto error;
    }

    ngx_escape_uri(s->data, cert.data, cert.len, NGX_ESCAPE_URI_COMPONENT);

    success = 1;
error:
    if (cert.data) {
        BSL_SAL_FREE(cert.data);
    }
    if (!success) {
        s->data = (unsigned char *) "";
        s->len = 1;
    }
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_subject_dn(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    HITLS_CERT_X509 *clientCert = HITLS_GetPeerCertificate(c->ssl->connection);
    if (clientCert == NULL) {
        return NGX_ERROR;
    }
    BSL_Buffer dnName = {NULL, 0};
    int32_t ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_SUBJECT_DN_STR, &dnName, sizeof(BSL_Buffer));
    if (ret != HITLS_X509_SUCCESS) {
        goto ERR;
    }

    s->data = ngx_pnalloc(pool, dnName.dataLen);
    if (s->data == NULL) {
        goto ERR;
    }
    s->len = dnName.dataLen;
    ngx_memcpy(s->data, dnName.data, dnName.dataLen);
 
ERR:
    HITLS_X509_CertFree(clientCert);
    BSL_SAL_FREE(dnName.data);
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_issuer_dn(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    HITLS_CERT_X509 *clientCert = HITLS_GetPeerCertificate(c->ssl->connection);
    if (clientCert == NULL) {
        return NGX_ERROR;
    }

    BSL_Buffer dnName = {NULL, 0};
    int32_t ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_ISSUER_DN_STR, &dnName, sizeof(BSL_Buffer));
    if (ret != HITLS_X509_SUCCESS) {
        goto ERR;
    }

    s->data = ngx_pnalloc(pool, dnName.dataLen);
    if (s->data == NULL) {
        goto ERR;
    }
    s->len = dnName.dataLen;
    ngx_memcpy(s->data, dnName.data, dnName.dataLen);

ERR:
    HITLS_X509_CertFree(clientCert);
    BSL_SAL_FREE(dnName.data);
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_subject_dn_legacy(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_issuer_dn_legacy(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_serial_number(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    int success = 0;

    HITLS_CERT_X509 *clientCert = HITLS_GetPeerCertificate(c->ssl->connection);
    if (clientCert == NULL) {
        return NGX_ERROR;
    }

    BSL_Buffer num = { NULL, 0 };
    int32_t ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_SERIALNUM, &num, sizeof(num));
    if (ret != HITLS_X509_SUCCESS) {
        goto error;
    }

    s->data = ngx_pnalloc(pool, num.dataLen);
    s->len = num.dataLen;
    ngx_memcpy(s->data, num.data, num.dataLen);
    success = 1;
error:
    if (clientCert != NULL) {
        HITLS_X509_CertFree(clientCert);
    }
    if (!success) {
        s->data = (unsigned char *)"";
        s->len = 1;
    }
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_fingerprint(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_client_verify(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}
ngx_int_t
ngx_ssl_get_client_v_start(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    HITLS_CERT_X509 *clientCert = HITLS_GetPeerCertificate(c->ssl->connection);
    if (clientCert == NULL) {
        return NGX_ERROR;
    }

    BSL_Buffer time = { NULL, 0 };
    int32_t ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_BEFORE_TIME, &time, sizeof(BSL_Buffer));
    if (ret != HITLS_X509_SUCCESS) {
        goto error;
    }
    s->data = ngx_pnalloc(pool, time.dataLen);
    s->len = time.dataLen;
    ngx_memcpy(s->data, time.data, time.dataLen);

error:
    HITLS_X509_CertFree(clientCert);
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_client_v_end(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{

    HITLS_CERT_X509 *clientCert = HITLS_GetPeerCertificate(c->ssl->connection);
    if (clientCert == NULL) {
        return NGX_ERROR;
    }

    BSL_Buffer time = { NULL, 0 };
    int32_t ret = HITLS_X509_CertCtrl(clientCert, HITLS_X509_GET_AFTER_TIME, &time, sizeof(BSL_Buffer));
    if (ret != HITLS_X509_SUCCESS) {
        goto error;
    }
    s->data = ngx_pnalloc(pool, time.dataLen);
    s->len = time.dataLen;
    ngx_memcpy(s->data, time.data, time.dataLen);

error:
    HITLS_X509_CertFree(clientCert);
    return NGX_OK;
}

ngx_int_t
ngx_ssl_get_client_v_remain(ngx_connection_t *c, ngx_pool_t *pool, ngx_str_t *s)
{
    fprintf(stderr, "MISSING get** implementation.\n");
    return NGX_OK;
}

ngx_int_t
ngx_ssl_client_session_cache(ngx_conf_t *cf, ngx_ssl_t *ssl, ngx_uint_t enable)
{
    if (HITLS_CFG_SetSessionCacheMode(ssl->ctx, HITLS_SESS_CACHE_CLIENT) != HITLS_SUCCESS) {
        return NGX_ERROR;
    }
    return NGX_OK;
}
